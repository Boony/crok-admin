<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin-panel'], function () {
    Route::get('/', function () {
        return view('admin.index');
    });

    Route::get('/login', function () {
        return view('admin.login');
    });

    Route::get('/recovery', function () {
        return view('admin.password-recovery');
    });

    Route::get('/lock', function () {
        return view('admin.lock');
    });


});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
